// IMPORTS

const mongoose = require("mongoose");

// DEFINE SCHEMA
const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    email: {
      type: String,
      unique: true,
    },
    password: {
      type: String,
    },
    role: {
      type: String,
    },
  }
  // { collection: "User" }
);

// CREATE User MODEL
const User = mongoose.model("User", userSchema);

// EXPORT User
module.exports = User;
