// IMPORTS

const mongoose = require("mongoose");

// DEFINE SCHEMA
const accountSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "An account must have a name"],
    // unique: true,
  },
  balance: {
    type: Number,
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
  },

  createdAtUtc: { type: Date, default: Date.now },
});

// CREATE ACCOUNT MODEL
const Account = mongoose.model("Account", accountSchema);

// EXPORT ACCOUNT
module.exports = Account;
