const catchAsync = require("../../util/catchAsync");
const AppError = require("../../util/appError");
const User = require("../../models/userModel");
const sendResponse = require("../../util/sendResponse");

exports.createUser = catchAsync(async (req, res, next) => {
  const data = req.body;
  const user = await User.create(data);
  sendResponse(res, null, 200, user);
});

exports.getAllUsers = catchAsync(async (req, res, next) => {
  const users = await User.find();

  sendResponse(res, users.length, 200, users);
});
