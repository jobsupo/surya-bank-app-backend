const express = require("express");
const router = express.Router();
const app = express();

const userRouter = require("./userRoutes");
const accountRouter = require("./accountRoutes");
const transactionRouter = require("./transactionRoutes");

app.use("/transactions", transactionRouter);
app.use("/users", userRouter);
app.use("/accounts", accountRouter);

module.exports = app;
